from OpenGL import GL as gl
import glfw
import math

class BaseWidget:
    def __init__(self, **kwargs):
        # 初始化基础组件
        self.children = []
        self.styles = kwargs.get('styles', {})
        self.x = kwargs.get('x', 0)
        self.y = kwargs.get('y', 0)
        self.width = kwargs.get('width', 100)
        self.height = kwargs.get('height', 50)
        self.hover = False  # 新增：悬停状态
        self.active = False  # 新增：激活状态
        self.shadow = self.styles.get('shadow', False)  # 新增：阴影
        self.rounded = self.styles.get('rounded', False)  # 新增：圆角

    def render(self):
        # 渲染组件（基础渲染逻辑）
        if self.shadow:
            self.render_shadow()
        if self.rounded:
            self.render_rounded_corners()
        # ... existing render logic ...

    def render_shadow(self):
        # 新增：渲染阴影
        gl.glColor4f(0.0, 0.0, 0.0, 0.5)  # 阴影颜色
        gl.glBegin(gl.GL_QUADS)
        gl.glVertex2f(self.x + 5, self.y - 5)
        gl.glVertex2f(self.x + self.width + 5, self.y - 5)
        gl.glVertex2f(self.x + self.width + 5, self.y + self.height + 5)
        gl.glVertex2f(self.x + 5, self.y + self.height + 5)
        gl.glEnd()

    def render_rounded_corners(self):
        # 新增：渲染圆角（改进实现）
        gl.glColor3f(1.0, 1.0, 1.0)  # 圆角颜色
        for corner in ['top_left', 'top_right', 'bottom_left', 'bottom_right']:
            self.draw_rounded_corner(corner, self.styles.get('border_radius', 10))

    def draw_rounded_corner(self, corner, radius):
        # 改进的四分之一圆绘制，增加更多细分角度以实现更平滑的圆角
        gl.glBegin(gl.GL_TRIANGLE_FAN)
        cx, cy = self.get_corner_position(corner)
        gl.glVertex2f(cx, cy)
        for angle in range(0, 91, 5):
            rad = math.radians(angle)
            if corner == 'top_left':
                x = cx - radius * math.cos(rad)
                y = cy + radius * math.sin(rad)
            elif corner == 'top_right':
                x = cx + radius * math.cos(rad)
                y = cy + radius * math.sin(rad)
            elif corner == 'bottom_left':
                x = cx - radius * math.cos(rad)
                y = cy - radius * math.sin(rad)
            elif corner == 'bottom_right':
                x = cx + radius * math.cos(rad)
                y = cy - radius * math.sin(rad)
            gl.glVertex2f(x, y)
        gl.glEnd()

    def get_corner_position(self, corner):
        if corner == 'top_left':
            return (self.x + self.styles.get('border_radius', 5), self.y + self.height - self.styles.get('border_radius', 5))
        elif corner == 'top_right':
            return (self.x + self.width - self.styles.get('border_radius', 5), self.y + self.height - self.styles.get('border_radius', 5))
        elif corner == 'bottom_left':
            return (self.x + self.styles.get('border_radius', 5), self.y + self.styles.get('border_radius', 5))
        elif corner == 'bottom_right':
            return (self.x + self.width - self.styles.get('border_radius', 5), self.y + self.styles.get('border_radius', 5))

    def update_state(self, cursor_x, cursor_y, mouse_button, action):
        # 新增：更新组件状态
        if self.x <= cursor_x <= self.x + self.width and self.y <= cursor_y <= self.y + self.height:
            self.hover = True
            if mouse_button == glfw.MOUSE_BUTTON_LEFT:
                if action == glfw.PRESS:
                    self.active = True
                elif action == glfw.RELEASE:
                    self.active = False
        else:
            self.hover = False
            self.active = False

    def add_child(self, widget):
        self.children.append(widget)