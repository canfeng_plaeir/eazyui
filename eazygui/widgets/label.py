from .base_widget import BaseWidget
import OpenGL.GL as gl

class Label(BaseWidget):
    def __init__(self, text, **kwargs):
        super().__init__(**kwargs)
        self.text = text
        self.styles['color'] = self.styles.get('color', (0, 0, 0))

    def render(self):
        # 渲染标签文本
        gl.glColor3f(*self.styles['color'])  # 设置文本颜色
        gl.glRasterPos2f(self.x, self.y)
        for char in self.text:
            gl.glDrawPixels(8, 13, gl.GL_RGB, gl.GL_UNSIGNED_BYTE, self.create_character(char))

    def create_character(self, char):
        # 创建一个简单的点阵字符
        char_pixels = []
        for row in range(13):
            for col in range(8):
                if self.is_pixel_set(char, col, row):
                    char_pixels.extend([0, 0, 0])  # 黑色像素
                else:
                    char_pixels.extend([255, 255, 255])  # 白色像素
        return bytes(char_pixels)

    def is_pixel_set(self, char, x, y):
        # 简单的字符点阵定义（仅支持大写字母和数字）
        patterns = {
            'A': [
                "  ██  ",
                " ████ ",
                "██  ██",
                "██████",
                "██  ██"
            ],
            # 添加更多字符的点阵定义...
        }
        if char.upper() in patterns:
            return patterns[char.upper()][y][x] == '█'
        return False