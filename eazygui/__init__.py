from .core import EazyGUI
from .widgets import Button, Label

__all__ = ['EazyGUI', 'Button', 'Label']