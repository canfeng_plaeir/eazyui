class Animator:
    def __init__(self):
        # 初始化动画管理器
        self.animations = []

    def animate(self, target, properties, duration):
        # 实现动画逻辑
        animation = {
            'target': target,
            'properties': properties,
            'duration': duration,
            'elapsed': 0  # 新增用于跟踪动画进度
        }
        self.animations.append(animation)

    def update(self, delta_time):
        # 更新所有动画
        for animation in self.animations[:]:
            animation['elapsed'] += delta_time
            progress = animation['elapsed'] / animation['duration']
            if progress >= 1:
                progress = 1
            for prop, end_value in animation['properties'].items():
                start_value = getattr(animation['target'], prop, 0)
                new_value = start_value + (end_value - start_value) * progress
                setattr(animation['target'], prop, new_value)
            if progress >= 1:
                self.animations.remove(animation)