from eazygui.widgets.button import Button
from eazygui.widgets.label import Label
import glfw
import OpenGL.GL as gl

def on_button_click():
    print("Button clicked!")

def main():
    if not glfw.init():
        return

    window = glfw.create_window(640, 480, "Button and Label Example", None, None)
    if not window:
        glfw.terminate()
        return

    glfw.make_context_current(window)

    button = Button("Click Me", x=100, y=100, width=200, height=50)
    button.on_click(on_button_click)

    label = Label("Hello, World!", x=100, y=200)

    while not glfw.window_should_close(window):
        gl.glClear(gl.GL_COLOR_BUFFER_BIT)

        button.render()
        label.render()

        glfw.swap_buffers(window)
        glfw.poll_events()

    glfw.terminate()

if __name__ == "__main__":
    main()